#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      fer
#
# Created:     22/12/2016
# Copyright:   (c) fer 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from Objetos import*
import time

import pygame
from pygame.locals import *
from configuracion import *
from Objetos import *




class Configuracion():
    def __init__(self, director):
        self.director = director
        self.fondo = cargar_imagen("Escenas\\MenuInicio\\Imagenes\\fondo.png", False)
        self.botonJugar = Boton(391,496,"Escenas\\MenuInicio\\Imagenes\\botonJugar.png","Escenas\\MenuInicio\\Imagenes\\botonJugar1.png","Escenas\\MenuInicio\\Sonidos\\jugarSonido.wav",False)
        self.botonCreditos = Boton(134,496,"Escenas\\MenuInicio\\Imagenes\\botonCreditos.png","Escenas\\MenuInicio\\Imagenes\\botonCreditos1.png","Escenas\\MenuInicio\\Sonidos\\creditos.wav",False)
        self.botonConfiguraciones = Boton(654,496,"Escenas\\MenuInicio\\Imagenes\\botonConfig.png","Escenas\\MenuInicio\\Imagenes\\botonConfig1.png","Escenas\\MenuInicio\\Sonidos\\configuraciones.wav",False)
        self.musica = "Escenas\\MenuInicio\\Sonidos\\Musica.wav"
        self.tiempo = 0

    def actualizacion(self,pantalla):
        pass


    def en_evento(self):
        if(self.botonJugar.es_click()):
            self.botonJugar.reproducir()
        if(self.botonCreditos.es_click()):
            self.botonCreditos.reproducir()
        if(self.botonConfiguraciones.es_click()):
            self.botonConfiguraciones.reproducir()

        if(self.botonJugar.es_pisado()):
            self.botonJugar.cambiar(True)
        else:
            self.botonJugar.cambiar(False)
        if(self.botonCreditos.es_pisado()):
            self.botonCreditos.cambiar(True)
        else:
            self.botonCreditos.cambiar(False)
        if(self.botonConfiguraciones.es_pisado()):
            self.botonConfiguraciones.cambiar(True)
        else:
            self.botonConfiguraciones.cambiar(False)



    def dibujo(self, pantalla):
        pantalla.blit(self.fondo,(0,0))
        pantalla.blit(self.botonJugar.image,self.botonJugar.rect)
        pantalla.blit(self.botonCreditos.image,self.botonCreditos.rect)
        pantalla.blit(self.botonConfiguraciones.image,self.botonConfiguraciones.rect)


    def reproducir(self):
        pygame.mixer.music.load(self.musica)
        pygame.mixer.music.play()

class Secundarias():
    def __init__(self, director):
        self.director = director
        self.fondo = cargar_imagen("Escenas\\MenuInicio\\Imagenes\\fondo.png", False)
        self.botonJugar = Boton(391,496,"Escenas\\MenuInicio\\Imagenes\\botonJugar.png","Escenas\\MenuInicio\\Imagenes\\botonJugar1.png","Escenas\\MenuInicio\\Sonidos\\jugarSonido.wav",False)
        self.botonCreditos = Boton(134,496,"Escenas\\MenuInicio\\Imagenes\\botonCreditos.png","Escenas\\MenuInicio\\Imagenes\\botonCreditos1.png","Escenas\\MenuInicio\\Sonidos\\creditos.wav",False)
        self.botonConfiguraciones = Boton(654,496,"Escenas\\MenuInicio\\Imagenes\\botonConfig.png","Escenas\\MenuInicio\\Imagenes\\botonConfig1.png","Escenas\\MenuInicio\\Sonidos\\configuraciones.wav",False)
        self.musica = "Escenas\\MenuInicio\\Sonidos\\Musica.wav"
        self.tiempo = 0

    def actualizacion(self,pantalla):
        pass


    def en_evento(self):
        if(self.botonJugar.es_click()):
            self.botonJugar.reproducir()
        if(self.botonCreditos.es_click()):
            self.botonCreditos.reproducir()
        if(self.botonConfiguraciones.es_click()):
            self.botonConfiguraciones.reproducir()

        if(self.botonJugar.es_pisado()):
            self.botonJugar.cambiar(True)
        else:
            self.botonJugar.cambiar(False)
        if(self.botonCreditos.es_pisado()):
            self.botonCreditos.cambiar(True)
        else:
            self.botonCreditos.cambiar(False)
        if(self.botonConfiguraciones.es_pisado()):
            self.botonConfiguraciones.cambiar(True)
        else:
            self.botonConfiguraciones.cambiar(False)



    def dibujo(self, pantalla):
        pantalla.blit(self.fondo,(0,0))
        pantalla.blit(self.botonJugar.image,self.botonJugar.rect)
        pantalla.blit(self.botonCreditos.image,self.botonCreditos.rect)
        pantalla.blit(self.botonConfiguraciones.image,self.botonConfiguraciones.rect)


    def reproducir(self):
        pygame.mixer.music.load(self.musica)
        pygame.mixer.music.play()







